<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<meta charset="ISO-8859-1">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<title>Insert title here</title>
	
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	
	<link rel="stylesheet" type="text/css" href="css/fullpage.min.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />

	<!--[if IE]>
		<script type="text/javascript">
			 var console = { log: function() {} };
		</script>
	<![endif]-->

</head>
<body>

<nav id="header">
	<div class="nav-wrapper white">
		<a href="#!" class="brand-logo"><img src="imgs/logo_alpha_270x.png" class="logo" alt="UNaLab logo"></a>
		<!--
		<ul class="right">
			<li><a class="grey-text text-darken-4" >Create ONIA Problem</a></li>
		</ul>
		-->
	</div>
</nav>

<div id="fullpage">
	<div class="section" id="section0">
		<div class="slide" id="slide1">
			<div class="container">	
				<div class="wrap">
					<h1>This is like a normal website.</h1>
				
					<h1> Rest of your site </h1>
				
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum a sit amet nulla. Donec sed tellus tempor ligula condimentum placerat. Vivamus placerat magna nisi, eu elementum risus imperdiet sit amet. Nunc varius dictum porttitor. Morbi rhoncus magna in quam fringilla fringilla in in odio. Quisque fringilla ante vitae tellus fringilla, condimentum tristique mi pharetra. Aenean ultricies odio at erat facilisis tincidunt. Fusce tempor auctor justo, nec facilisis quam vehicula vel. Aenean non mattis purus, eget lobortis odio. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam cursus elit nec aliquam consequat.
					</p>
					<p>
					Nullam sem orci, tincidunt non lorem quis, ultricies blandit nisl. Donec eget sollicitudin tortor. Integer ut orci sit amet ipsum porta feugiat sit amet ut nulla. Vestibulum auctor, tortor sed scelerisque consectetur, nisl tellus placerat tortor, non euismod nisi risus vitae ipsum. Morbi a velit purus. Nam euismod porta sapien, sed scelerisque nulla lacinia vitae. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam at eleifend ligula, in eleifend sapien.
					</p>
					<div class="box">
					In elementum nec quam et eleifend. Ut nec erat fermentum, mattis leo non, fringilla tellus. Integer at dui nibh. Etiam facilisis fermentum turpis. Nulla malesuada iaculis nisl, ac accumsan felis pulvinar ut. Proin porttitor nulla libero, vel tristique erat faucibus quis. Aliquam pharetra enim et sapien bibendum, interdum viverra lectus sagittis. Vivamus pharetra, risus quis malesuada interdum, mi mauris dignissim mi, ac hendrerit orci nulla vel felis. Vivamus dapibus, nisi vel viverra tincidunt, ligula nunc sagittis elit, eget lacinia tellus velit et leo. Curabitur a tortor pretium, aliquam justo gravida, commodo ipsum. In fermentum lorem eu tincidunt consequat. Donec nec blandit ipsum, id scelerisque velit. Aenean vel ultrices ligula, at imperdiet dolor. Sed euismod turpis et nibh adipiscing feugiat. Etiam diam leo, sollicitudin eu suscipit non, laoreet sit amet dui. In augue purus, semper in blandit ut, suscipit vehicula tortor.
					</div>
					<p>
					Praesent id varius neque. Nunc risus elit, tincidunt eu nulla vitae, adipiscing porta nibh. Pellentesque dignissim dolor ligula, eu vestibulum justo elementum ac. In a risus ullamcorper, iaculis lectus non, condimentum elit. Pellentesque ac felis nec mauris venenatis elementum. In porttitor mauris sit amet posuere scelerisque. Nunc interdum arcu sit amet libero fermentum, nec consequat risus dignissim.
					</p>
					<p>
					Curabitur dui elit, tristique eget venenatis et, scelerisque mattis arcu. Pellentesque lectus orci, tempus in enim et, condimentum rutrum magna. Mauris nec eros viverra, facilisis nibh ut, sodales urna. Pellentesque nec neque in ipsum imperdiet egestas vitae vel augue. Integer felis eros, dictum at eleifend aliquet, lacinia non neque. Curabitur eget condimentum urna, eget sodales lectus. Maecenas blandit ac neque id elementum. Phasellus ultricies vestibulum elit ut sagittis. Nam ut porta mi.
					</p>
				</div>
			</div>
		</div>

	
	    <div class="slide" id="slide2">
			<div class="container">
				<h1>Select a city</h1>
				
				  <div class="row">
				  
				    <div class="col s12 m4">
				      <div class="card">
				        <div class="card-image">
				          <img src="imgs/Eindhoven.jpg">
				          <span class="card-title">Eindhoven</span>
				        </div>
				        <div class="card-content">
				          <p>
						    <label>
						      <input class="with-gap" name="group3" type="radio"  />
						      <span>Eindhoven city</span>
						    </label>
						  </p>
				        </div>
				      </div>
				    </div>
				    
				    <div class="col s12 m4">
				      <div class="card">
				        <div class="card-image">
				          <img src="imgs/Tampere.jpg">
				          <span class="card-title">Tampere</span>
				        </div>
				        <div class="card-content">
				          <p>
						    <label>
						      <input class="with-gap" name="group3" type="radio"  />
						      <span>Tampere city</span>
						    </label>
						  </p>
				        </div>
				      </div>
				    </div>
				    				    
				    <div class="col s12 m4">
				      <div class="card">
				        <div class="card-image">
				          <img src="imgs/Genova.jpg">
				          <span class="card-title">Genova</span>
				        </div>
				        <div class="card-content">
				          <p>
						    <label>
						      <input class="with-gap" name="group3" type="radio"  />
						      <span>Genova city</span>
						    </label>
						  </p>
				        </div>
				      </div>
				    </div>
				    
				 </div>
				  
			</div>
		</div>

	    <div class="slide" id="slide3">
	    	<div class="intro">
		    	<div class="container">
					<h1>Current situation in your city</h1>
			
					<iframe width="1024" height="692" src="http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&amp;OBJECT_LABEL=USG_LandUse2&amp;TOOLBAR_VISIBLE=true&amp;ORGANIZATION=DEFAULT_TENANT" frameborder="0"> </iframe>
					
					<h1>Select the kind of impact to be evaluated</h1>
					
					  <div class="row">
					  
					    <div class="col s12 m4">
					      <div class="card">
					        <div class="card-image">
					          <img src="https://via.placeholder.com/350x150">
					          <span class="card-title">Heat</span>
					        </div>
					        <div class="card-content">
					          <p>
							    <label>
							      <input class="with-gap" name="group3" type="radio"  />
							      <span>Urban heat</span>
							    </label>
							  </p>
					        </div>
					      </div>
					    </div>
					    
					    <div class="col s12 m4">
					      <div class="card">
					        <div class="card-image">
					          <img src="https://via.placeholder.com/350x150">
					          <span class="card-title">Sprawl</span>
					        </div>
					        <div class="card-content">
					          <p>
							    <label>
							      <input class="with-gap" name="group3" type="radio"  />
							      <span>Urban sprawl</span>
							    </label>
							  </p>
					        </div>
					      </div>
					    </div>
					    				    
					    <div class="col s12 m4">
					      <div class="card">
					        <div class="card-image">
					          <img src="https://via.placeholder.com/350x150">
					          <span class="card-title">...</span>
					        </div>
					        <div class="card-content">
					          <p>
							    <label>
							      <input class="with-gap" name="group3" type="radio"  />
							      <span>Urban ...</span>
							    </label>
							  </p>
					        </div>
					      </div>
					    </div>
					    
					 </div>
				</div>
			</div>
		</div>

	    <div class="slide" id="slide4" data-anchor="slide4">
			<div class="intro">
				<div class="container">	
					<div class="wrap">
						<h1>This is like a normal website.</h1>
					
						Just place the rest of your page after the fullpage wrapper and use the option `fitToSection:false` and `autoScrolling:false`. And enjoy a great single slider.
						<script src="https://gist.github.com/alvarotrigo/368e6d6ad3c5c0e98d3c.js"></script>
						<script src="https://gist.github.com/alvarotrigo/c74366b59ecba2f9c3da.js"></script>
					
						<h1> Rest of your site </h1>
					
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum a sit amet nulla. Donec sed tellus tempor ligula condimentum placerat. Vivamus placerat magna nisi, eu elementum risus imperdiet sit amet. Nunc varius dictum porttitor. Morbi rhoncus magna in quam fringilla fringilla in in odio. Quisque fringilla ante vitae tellus fringilla, condimentum tristique mi pharetra. Aenean ultricies odio at erat facilisis tincidunt. Fusce tempor auctor justo, nec facilisis quam vehicula vel. Aenean non mattis purus, eget lobortis odio. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam cursus elit nec aliquam consequat.
						</p>
						<p>
						Nullam sem orci, tincidunt non lorem quis, ultricies blandit nisl. Donec eget sollicitudin tortor. Integer ut orci sit amet ipsum porta feugiat sit amet ut nulla. Vestibulum auctor, tortor sed scelerisque consectetur, nisl tellus placerat tortor, non euismod nisi risus vitae ipsum. Morbi a velit purus. Nam euismod porta sapien, sed scelerisque nulla lacinia vitae. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam at eleifend ligula, in eleifend sapien.
						</p>
						<div class="box">
						In elementum nec quam et eleifend. Ut nec erat fermentum, mattis leo non, fringilla tellus. Integer at dui nibh. Etiam facilisis fermentum turpis. Nulla malesuada iaculis nisl, ac accumsan felis pulvinar ut. Proin porttitor nulla libero, vel tristique erat faucibus quis. Aliquam pharetra enim et sapien bibendum, interdum viverra lectus sagittis. Vivamus pharetra, risus quis malesuada interdum, mi mauris dignissim mi, ac hendrerit orci nulla vel felis. Vivamus dapibus, nisi vel viverra tincidunt, ligula nunc sagittis elit, eget lacinia tellus velit et leo. Curabitur a tortor pretium, aliquam justo gravida, commodo ipsum. In fermentum lorem eu tincidunt consequat. Donec nec blandit ipsum, id scelerisque velit. Aenean vel ultrices ligula, at imperdiet dolor. Sed euismod turpis et nibh adipiscing feugiat. Etiam diam leo, sollicitudin eu suscipit non, laoreet sit amet dui. In augue purus, semper in blandit ut, suscipit vehicula tortor.
						</div>
						<p>
						Praesent id varius neque. Nunc risus elit, tincidunt eu nulla vitae, adipiscing porta nibh. Pellentesque dignissim dolor ligula, eu vestibulum justo elementum ac. In a risus ullamcorper, iaculis lectus non, condimentum elit. Pellentesque ac felis nec mauris venenatis elementum. In porttitor mauris sit amet posuere scelerisque. Nunc interdum arcu sit amet libero fermentum, nec consequat risus dignissim.
						</p>
						<p>
						Curabitur dui elit, tristique eget venenatis et, scelerisque mattis arcu. Pellentesque lectus orci, tempus in enim et, condimentum rutrum magna. Mauris nec eros viverra, facilisis nibh ut, sodales urna. Pellentesque nec neque in ipsum imperdiet egestas vitae vel augue. Integer felis eros, dictum at eleifend aliquet, lacinia non neque. Curabitur eget condimentum urna, eget sodales lectus. Maecenas blandit ac neque id elementum. Phasellus ultricies vestibulum elit ut sagittis. Nam ut porta mi.
						</p>			
					
					</div>
				</div><!-- container -->
			</div>
<!-- 			intro -->
		</div>
<!-- 		slide -->
	</div>
<!-- 	section -->
</div>
<!-- fullpage -->




	<!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="vendors/scrolloverflow.min.js"></script>
<!--     <script type="text/javascript" src="vendors/jquery.slimscroll.min.js"></script> -->
	<script type="text/javascript" src="js/fullpage.min.js"></script>
<!-- 	<script type="text/javascript" src="examples.js"></script> -->
	
	<script type="text/javascript">
	
	    var myFullpage = new fullpage('#fullpage', {
	        anchors: ['firstPage', 'secondPage', 'thirdPage'],
	        sectionsColor: ['#dcedc8', '#dcedc8', '#dcedc8'],
	        slidesNavigation: true,
	        autoScrolling: false,
	        css3: true,
	        fitToSection: false,
	        scrollOverflow: true
	    });
	</script>

</body>
</html>