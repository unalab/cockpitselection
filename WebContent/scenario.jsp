<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<meta charset="ISO-8859-1">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<title>NBS SVT tool</title>
	
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	
	<link rel="stylesheet" type="text/css" href="css/fullpage.min.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />

	<!--[if IE]>
		<script type="text/javascript">
			 var console = { log: function() {} };
		</script>
	<![endif]-->

</head>
<body>

	<nav id="header">
		<div class="nav-wrapper white">
			<a href="index.jsp" class="brand-logo"><img src="imgs/logo_alpha_270x.png" class="logo" alt="UNaLab logo"></a>
			
			<span class="brand-logo secondary-text" style="margin-left: 130px; margin-top: 12px;">NBS Simulation Visualization Tool</span>
		</div>
	</nav>

	<div class="section">
		<div class="row">
			<div class="col s12 m2">
				<a class="waves-effect waves-light btn-small left" href="index.jsp">
					<i class="material-icons left">navigate_before</i>Reset parameters
				</a>
			</div>
			<div id="resume" class="col s12 m8">
				<div class="chip white">
				    <i class="material-icons left secondary white-text">location_city</i><span class="cityTitle"></span>
				</div>
				<div class="chip white">
				    <i class="material-icons left secondary white-text">filter_1</i><span class="impactTitle"></span>
				</div>
				<div class="chip white">
				    <i class="material-icons left secondary white-text">filter_2</i><span class="indicatorTitle"></span>
				</div>
			</div>
			
			<div class="col s12 m2">	
				 <!-- Dropdown Trigger -->
				  <a id="scnSelect" class='dropdown-trigger btn left' href='#' data-target='dropdownScenari'>Choose a scenario</a>
				  <!-- Dropdown Structure -->
				  <ul id='dropdownScenari' class='dropdown-content' style="max-width: 80%;">
				  </ul>
			 </div>
		 </div>
	</div>
	<div class="section" style="padding: 15px;">
		<!-- Cockpit Template -->
		<iframe class="cockpitIframe hide" width="100%" height="692" src="http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&amp;OBJECT_LABEL=USG_LandUse2&amp;TOOLBAR_VISIBLE=true&amp;ORGANIZATION=DEFAULT_TENANT" style="border:none;"> </iframe>
		
		<!-- <div class="row">
		   <div class="col s12 m3 input-field">
			    <select id="scnSelect">
			      <option id="optscn1" value="scn1">Scenario 1</option>
			      <option id="optscn2" value="scn2">Scenario 2</option>
			      <option id="optscn3" value="scn3">Scenario 3</option>
			      <option id="optscn4" value="scn4" disabled>Scenario 4</option>
			      <option id="optscn5" value="scn5" disabled>Scenario 5</option>
			    </select>
	   			<label>Change the Scenario</label>		
			</div>
		</div> -->
		
<!-- 	<div class="row">
		<div class="col s12 m6">	
			 Dropdown Trigger
			  <a class='dropdown-trigger btn left' href='#' data-target='dropdownScenari'>Choose a scenario</a>
			  Dropdown Structure
			  <ul id='dropdownScenari' class='dropdown-content' style="max-width: 80%;">
			  </ul>
		 </div>
	 </div> -->
	
		<div id="multiCockpit" class="row viewCockpit">
			<div class="col s12 m6">
				<div class="cockpitBox"></div>
			</div>
			
			<div class="col s12 m6">
			    <div class="col s12">
			      <ul class="tabs">
			        <li class="tab tab1 col s4"><a class="tabName" href="#tab1">Cockpit 1</a></li>
			        <li class="tab tab2 col s4"><a class="tabName" href="#tab2">Cockpit 2</a></li>
			        <li class="tab tab3 col s4"><a class="tabName" href="#tab3">Cockpit 3</a></li>
			      </ul>
			    </div>
			    <div id="tab1" class="col s12 tabContainer"></div>
			    <div id="tab2" class="col s12 tabContainer"></div>
			    <div id="tab3" class="col s12 tabContainer"></div>
			</div>
	
		</div>
		<div id="singleCockpit" class="row viewCockpit">
			<div class="col s12">
				<div class="cockpitBox"></div>
			</div>
		</div>
		<div class="spacer"></div>
	</div>	

	<footer class="valign-wrapper primary">
		<div class="center white-text">&copy; UNALAB project</div>
	</footer>

	<!--JavaScript at end of body for optimized loading-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script type="text/javascript" src="js/variable.js"></script>
    <script type="text/javascript" src="js/global.js"></script>
    
    
</body>
</html>