$( window ).on( "load", function(){
	$(".overlap").hide();
});

var heightSlide = 0;

$(document).ready(function() {

	$('#fullpage').fullpage({
		licenseKey: 'SOURCE-GPLV3-LICENSE',
//		anchors: ['firstPage', 'secondPage', 'thirdPage', 'fourPage'],
//		sectionsColor: ['#dcedc8', '#dcedc8', '#dcedc8', '#dcedc8'],
//		anchors: ['firstPage'],
		sectionsColor: ['rgba(119,148,62,.2)'],
		loopHorizontal: false,
		paddingBottom: "50px",
		slidesNavigation: false,
		autoScrolling: false,
		css3: true,
		fitToSection: false,
//		scrollOverflow: true,
		scrollOverflowOptions: {
			scrollbars: false
		},
//		fixedElements: '#resume'

//Section-Slide0 (City)
		afterRender: function(){
			var pluginContainer = this;
			fullpage_api.setAllowScrolling(false,'right');
			if($("form#cityForm input[type=radio]").is(':checked')){
				fullpage_api.setAllowScrolling(true, 'right');
			} 
		},
		afterSlideLoad: function( section, origin, destination, direction){
			var loadedSlide = this;
			heightSlide = $("#section0").height();
			//Impact
			if(section.index == 0 && destination.index == 1){
				if($("form#impactForm input[type=radio]").is(':checked')){
					fullpage_api.setAllowScrolling(true,'right');
				}
				$("#section0").height("100%");
			}
			//Indicator
			if(section.index == 0 && destination.index == 2){
				var parametri = "";
				if($("form#indicatorForm input[type=radio]").is(':checked')){
					$("#viewScenarios").attr('href', 'scenario.jsp?scn=scn1'+parametri);
				}
				
				$("#section0").height("100%");
			}

		},
		onSlideLeave: function( section, origin, destination, direction){
			var leavingSlide = this;

			//leaving the 1st slide of the 1st Section to the right/left (city)
			if(section.index == 0 && origin.index == 0 && direction == 'right'){
				fullpage_api.setAllowScrolling(false,'right');
				city = $("form#cityForm input[type=radio]:checked").val();
				$(".cityTitle").text(city);
				createDescription(city);

			}else
				//leaving the 2nd slide of the 1st Section to the right/left (impact)

				if(section.index == 0 && origin.index == 1 && direction == 'right'){
					fullpage_api.setAllowScrolling(false,'right');
					impact = $("form#impactForm input[type=radio]:checked").val();
					$(".impactTitle").text(impact);
					if(impact == "Urban sprawl") {
						$(".hideUSprawl").css("display","none");
						showBaseScenario(paramsComb3);
						$("#viewScenariosTab1").on('click', function(){
							indicator = "Neighbourhood scale";
							console.log(indicator);
							$(".indicatorTitle").text(indicator);
							var parametri = '&city='+city+'&impact='+impact+'&indicator='+indicator;
							$("#viewScenariosTab1").attr('href', 'scenario.jsp?scn=scn1'+parametri);
						});
					}
					else{
						//chooseImpact(params.indicator);
						chooseIndicator("Local scale");
					}
				}else if(section.index == 0 && origin.index == 1 && direction == 'left'){
					fullpage_api.setAllowScrolling(true,'right');
					$("#section0").height(heightSlide);
				}else
					//leaving the 3rd slide of the 1st Section to the right/left (indicator)
					if(section.index == 0 && origin.index == 2 && direction == 'right'){
						//getIndicator();

					}else if(section.index == 0 && origin.index == 2 && direction == 'left'){
						fullpage_api.setAllowScrolling(true,'right');
						
						$("#section0").height(heightSlide);
					}

		}

	});


	$("form#cityForm input[type=radio], " +
	"form#impactForm input[type=radio]").on('change', function() {
		fullpage_api.setAllowScrolling(true,'right');
	});
	
	//parte chiara
	$("#viewScenariosTab1").on('click', function(){
		indicator = "Local scale";
		console.log(indicator);
		$(".indicatorTitle").text(indicator);
		var parametri = '&city='+city+'&impact='+impact+'&indicator='+indicator;
		$("#viewScenariosTab1").attr('href', 'scenario.jsp?scn=scn1'+parametri);
	});
	
	$("#viewScenariosTab2").on('click', function(){
		indicator = "Neighbourhood scale";
		console.log(indicator);
		$(".indicatorTitle").text(indicator);
		var parametri = '&city='+city+'&impact='+impact+'&indicator='+indicator;
		$("#viewScenariosTab2").attr('href', 'scenario.jsp?scn=scn1'+parametri);
	});
	
	$("#viewScenariosTab3").on('click', function(){
		indicator = "City scale";
		console.log(indicator);
		$(".indicatorTitle").text(indicator);
		var parametri = '&city='+city+'&impact='+impact+'&indicator='+indicator;
		$("#viewScenariosTab3").attr('href', 'scenario.jsp?scn=scn1'+parametri);
	});

	$(".tabName").on("click", function(e){
		selectedTab(e);

	})
});

function createDescription(city){
	switch (city) {
	case "Eindhoven":
		console.log('Eindhoven description');
		$("#intro_description").html(Eindhoven_intro);
		$("#modal-description").html(Eindhoven_description);
		break;
	case "Tampere":
		console.log('Tampere description');
		break;
	case "Genova":
		console.log('Genova description');
		break;
	default:
		console.log('No description');
	}
}


function selectedTab(event){
//	event.preventDefault();
//	event.stopPropagation();
	console.log($(event.currentTarget).html());
	chooseIndicator($(event.currentTarget).html());
	

}


//chiara
function chooseIndicator(indicator){
	switch (indicator) {
	case "Local scale":
		console.log('local indicator wins!');
		showBaseScenario(city+"_"+impact+"_"+indicator);

		break;
	case "Neighbourhood scale":
		console.log('Neighbourhood indicator wins!');
		showBaseScenario(city+"_"+impact+"_"+indicator);
		break;
	case "City scale":
		console.log("City indicator wins!");
		showBaseScenario(city+"_"+impact+"_"+indicator);
		break;
	default:
		console.log("nobody wins!");
	}
}

function showBaseScenario(param){
//	var $target = $('.cockpitIframe').clone();
	switch (param) { 
	case paramsComb1: 
		console.log('comb1 Wins!');
//		$('#tab1').appendTo();
		$('#cockpitIframe1').attr("src",uhg).removeClass("hide");
//		$target.clone().appendTo('#tab1').attr("src",USGsc3HOUSEHT);
		break;
	case paramsComb2: 
		console.log('comb2 Wins!');
		$('#cockpitIframe2').attr("src",uhn).removeClass("hide");
//		$('#singleCockpit .cockpitBox').appendTo($target.clone().attr("src",uhn.replace('selettore=x','selettore=Direct%20Impacts').replace('selettore_field_visible_description=x','selettore_field_visible_description=Direct%20Impacts')));
		break;
	case paramsComb3: 
//		console.log('comb4 Wins!');
		$('#cockpitIframe1').attr("src",usb).removeClass("hide");
		break;		
	/*case paramsComb4: 
		console.log('comb4 Wins!');
		$('#cockpitIframe2').attr("src",usb).removeClass("hide");
		break;*/
	case paramsComb13:
		console.log('comb13 Wins!');
		$('#cockpitIframe3').attr("src",uhcs).removeClass("hide");
//		$('#tab3').appendTo();
//		$target.clone().attr("src",uhcs).appendTo('#tab3')
	default:
		console.log('Nobody Wins!');
	}	
}