//Simulation parameter combinations/

//Eindhoven
var paramsComb1 = "Eindhoven_Urban heat_Local scale";
var paramsComb2 = "Eindhoven_Urban heat_Neighbourhood scale";
var paramsComb13 = "Eindhoven_Urban heat_City scale";
var paramsComb3 = "Eindhoven_Urban sprawl_Local scale";
var paramsComb4 = "Eindhoven_Urban sprawl_Neighbourhood scale";
var paramsComb14 = "Eindhoven_Urban sprawl_City scale";

//Tampere
var paramsComb5 = "Tampere_Urban heat_Local scale";
var paramsComb6 = "Tampere_Urban heat_Neighbourhood scale";
var paramsComb15 = "Tampere_Urban heat_City scale";
var paramsComb7 = "Tampere_Urban sprawl_Local scale";
var paramsComb8 = "Tampere_Urban sprawl_Neighbourhood scale";
var paramsComb16 = "Tampere_Urban sprawl_City scale";

//Genova
var paramsComb9 = "Genova_Urban heat_Local scale";
var paramsComb10 = "Genova_Urban heat_Neighbourhood scale";
var paramsComb17 = "Genova_Urban heat_City scale";
var paramsComb11 = "Genova_Urban sprawl_Local scale";
var paramsComb12 = "Genova_Urban sprawl_Neighbourhood scale";
var paramsComb18 = "Genova_Urban sprawl_City scale";



//paramsComb3
/* cockpit scenario 1 */
var USG_scn1LandUse = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=USG_scn1LandUse&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT"
var USG_scn1HouseHT = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=USG_scn1HouseHT&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT"
var USG_scenario1Q18 = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=USG_scenario1Q18&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=attributo=Development_Density(m%5E2%2Fgridcell)&attributo_field_visible_description=Development_Density(m%5E2%2Fgridcell)"
/* cockpit scenario 2 */
var USG_SC2_LANDUSE = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=USG_sc2_LandUse&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT";
var USG_SCN2HOUSEHT = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=USG_scn2HouseHT&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT";
var USGSC2_INDICATORS = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=USGsc2_Indicators&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=attributo=Housing_Quantity_(m%5E2%2Fhousehold)&attributo_field_visible_description=Housing_Quantity_(m%5E2%2Fhousehold)";
/*cockpit scenario 3 */
var USGsc3LandUse = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=USGsc3LandUse&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=attributo=Development_Density_(m%5E2%2Fgridcell)&attributo_field_visible_description=Development_Density_(m%5E2%2Fgridcell)";
var USGsc3HOUSEHT = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=USGsc3HouseHT&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT";
var USGsc3Indicators = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=t_USGsc3&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=attributo=Development_Density_(m%5E2%2Fgridcell)&attributo_field_visible_description=Development_Density_(m%5E2%2Fgridcell)"

/*cockpit scenario 4 */
var USGsc4LandUse = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=Scenario 4 - Land Use&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT"
var USGsc4HOUSEHT = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=Scenario 4 - House hold type&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT"
var USGsc4Indicators = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=sc4&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=attributo=Development_Density_(m%5E2%2Fgridcell)&attributo_field_visible_description=Development_Density_(m%5E2%2Fgridcell)"
	
//paramsComb4
var USNscAllinOne = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=USN_scAllInOne&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=attributo=SocEc_SX_2015_Neigh&attributo_field_visible_description=Scenario%20X"
	
//paramsComb3
var usb="http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=US_base_condriverb&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=attributo=Development_Density(m%2Fgridcell)&attributo_field_visible_description=Development_Density(m%2Fgridcell)"

//paramsComb1
var uhg = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=UHLocalBase&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT"
var uhls="http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=UHeatingLocal&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=attributo=xxx&attributo_field_visible_description=x"

//paramsComb2
var uhn = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=UHN_baseT&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT"
var uhns = "http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=UHNg&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=selettore=xxx&selettore_field_visible_description=x"
//paramsComb13
var uhcs = "http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=Urban Heath - CBS&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT"
var uhci = "http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=UHI&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=selettore=xxx&selettore_field_visible_description=x"
//DESCRIPTION SCENARI
var descriptionSprawl = {
	scn1:"Scenario1: Emmasingelkwadrant <br> <p>Located in the Witte Dame neighbourhood, this project is the biggest challenge of the 5 project areas. A new park will be created in the city centre of Eindhoven. This area consists of offices and a car park, which is privately owned. This will help bringin nature back to the city centre.</p>",
	scn2:"Scenario2: Frederika van Pruisenweg<br> <p>The Frederika van Pruisenweg is a very wide street with houses on both sides and a very wide green area in the middle. The municipality has the intention to transform this street by having the Gender flowing through the green area in the street, providing storm water control and recreation areas.</p>",
	scn3:"Scenario3: Gendervijver <br> <p>The Gendervijver at the border of the neighbourhoods of Engelsbergen and Hegenkamp is already a nice pond. Still the soil under the pond is polluted and needs cleaning. This gives the municipality a good opportunity to make the Gendervijver even nicer by promoting the ecological value and improving the water quality.</p>",
	scn4:"Scenario4: Stationsweg<br> <p>The Stationsweg is the last part of the Gender before it flows into the river Dommel. This part of the Gender is a challenge because the width of the public space is limited. The traffic department wants to reduce the amount of cars in the street, which will leave some space to create a visible Gender combined with green slopes.</p>",
//	scn5:"Scenario5: Willemstraat<br> <p>The Willemstraat is a part of the Gender alongside a very busy road with only little space left to make de Gender visible. The river will be kept undergound at the beginning of the street because of lack of space, and will be opened a bit further. New houses willbe built next to the newly opened river.</p>"
}

var descriptionHeat = {
		scn1:"Direct Impacts Scenario<br> <p>This scenario assesses the potential direct impacts of NBS scenario at the neighbourhood scale.</p>",
		scn2:"Indirects Impacts Scenario<br> <p>This scenario assesses the potential indirect impacts of NBS scenario at the neighbourhood scale.</p>",
		scn3:"Direct and Indirect Impacts Scenario<br> <p>This scenario assesses the potential direct and indirect impacts of NBS scenario at the neighbourhood scale.</p>",
//		scn4:"Scenario4<br> <p>The Stationsweg is the last part of the Gender before it flows into the river Dommel. This part of the Gender is a challenge because the width of the public space is limited. The traffic department wants to reduce the amount of cars in the street, which will leave some space to create a visible Gender combined with green slopes.</p>",
//		scn5:"Scenario5<br> <p>The Willemstraat is a part of the Gender alongside a very busy road with only little space left to make de Gender visible. The river will be kept undergound at the beginning of the street because of lack of space, and will be opened a bit further. New houses willbe built next to the newly opened river.</p>"
}

var descriptionHeatNeigh = {
		scn1:"Direct Impacts Scenario (absolute values)<br> <p>This scenario assesses the absolute values of potential direct impacts of NBS scenario at the neighbourhood scale.</p>",
		scn2:"Direct Impacts Scenario (difference values)<br> <p>This scenario assesses the difference values of potential direct impacts of NBS scenario at the neighbourhood scale.</p>",
		scn3:"Indirects Impacts Scenario (absolute values)<br> <p>This scenario assesses the absolute values of potential indirect impacts of NBS scenario at the neighbourhood scale.</p>",
		scn4:"Indirect Impacts Scenario (difference values)<br> <p>This scenario assesses the difference values of potential direct and indirect impacts of NBS scenario at the neighbourhood scale.</p>",
		scn5:"Direct and Indirect Scenario (absolute values)<br> <p>This scenario assesses the absolute values of potential direct and indirect impacts of NBS scenario at the neighbourhood scale.</p>",
		scn6:"Direct and Indirect Scenario (difference values)<br> <p>This scenario assesses the difference values of potential direct and indirect impacts of NBS scenario at the neighbourhood scale.</p>",
		
}

var descriptionSprawlN = {
		scn1:"Scenario1: Emmasingelkwadrant <br> <p>Located in the Witte Dame neighbourhood, this project is the biggest challenge of the 5 project areas. A new park will be created in the city centre of Eindhoven. This area consists of offices and a car park, which is privately owned. This will help bringin nature back to the city centre.</p>",
		scn2:"Scenario2: Frederika van Pruisenweg<br> <p>The Frederika van Pruisenweg is a very wide street with houses on both sides and a very wide green area in the middle. The municipality has the intention to transform this street by having the Gender flowing through the green area in the street, providing storm water control and recreation areas.</p>",
		scn3:"Scenario3: Gendervijver <br> <p>The Gendervijver at the border of the neighbourhoods of Engelsbergen and Hegenkamp is already a nice pond. Still the soil under the pond is polluted and needs cleaning. This gives the municipality a good opportunity to make the Gendervijver even nicer by promoting the ecological value and improving the water quality.</p>",
		scn4:"Scenario4: Stationsweg<br> <p>The Stationsweg is the last part of the Gender before it flows into the river Dommel. This part of the Gender is a challenge because the width of the public space is limited. The traffic department wants to reduce the amount of cars in the street, which will leave some space to create a visible Gender combined with green slopes.</p>",
		scn5:"Scenario5: Willemstraat<br> <p>The Willemstraat is a part of the Gender alongside a very busy road with only little space left to make de Gender visible. The river will be kept undergound at the beginning of the street because of lack of space, and will be opened a bit further. New houses willbe built next to the newly opened river.</p>"
	}

var Eindhoven_intro = 
		"<p>Eindhoven is facing problems related to water quality and quantity, as the urban water system did not keep track with the city's growth. " +
		"<a id='scnSelect' class='modal-trigger secondary darken-3 white-text' href='#modal1'>Read more</a> </p> "


var Eindhoven_description = 
		"<b>Current situation in Eindhoven</b>" +
		
		"<p class='left-align'>The main issues are: " +
		"<ul class='left-align'> " +
		"<li> - Water on streets after heavy rainfall due to insufficient urban drainage capacity;</li>" +
		"<li> - Combined sewer outlets polluting vulnerable surface waters with higher ecological functions and values;</li>" +
		"<li> - Malfunctioning of the remaining surface waters due to closing of former streams or connecting these to combined sewer systems;</li>" +
		"<li> - A large waste water treatment plant, serving 750,000 inhabitants, discharging effluent on a small surface water area, with insufficient biological treatment capacity in periods of large (diluted) sewage supply after heavy rainfall; and</li>" +
		"<li> - Groundwater entering basements of houses due to building in former wetlands, combined with reductions in groundwater extractions.</li>" +
		"</ul>" +
		
		"<p class='left-align'> A long term program has been developed to tackle these problems. The measures envisaged include:" +
		"<ul class='left-align'>" +
		"<li>changing the sewer system from combined to separate sewers; and</li>" +
		"<li>the (re-) opening of various watercourses throughout the city.</li></ul>" +
		"<br>" +
		"<p class='left-align'>Most of these measures will be implemented until 20XX; the date by which abovementioned problems will have been reduced significantly. In particular, one of the measures is the reopening of the Gender watercourse (which has been closed in the 1950s) in various parts of the city centre. The reopening of the Gender is divided in 5 project areas, each with their own specific challenges. " +
		
		"<p class='left-align'>The reopening of the &ldquo;Nieuwe Gender&rdquo; aims to:" +
		"<ul class='left-align'>" +
		"<li>Provide a technical optimal contribution to the water goals, including space for maximum storage and discharge capacity; and</li></p>" +
		"<li>Add maximum value to the public space (e.g. recreation and visibility).</li></ul>"
		
		

