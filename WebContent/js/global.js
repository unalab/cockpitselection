//GLOBAL VARS//
var city = '';
var impact = '';
var indicator = '';
var scenario = '';

//recupero parametri dal URL
var params={};
location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){params[k]=decodeURIComponent(v)});
var currentParam = params.city+"_"+params.impact+"_"+params.indicator;
console.log("currentParam: "+currentParam)
console.log(params)



$(document).ready(function() {
	$('.dropdown-trigger').dropdown({
		constrainWidth: false,
		alignment: "right",
		closeOnClick: true
	});
	
	$("#scnSelect option[value="+params.scn+"]").prop('selected', true);
//	$('select').formSelect();
//	$('select').on('change', function() {
//		console.log(location.href.replace(/=scn\d/gi, "="+this.value));
//		location.href = location.href.replace(/=scn\d/gi, "="+this.value);
//	});
    $('ul.tabs').tabs();
    $('.collapsible').collapsible();
	
	$(".cityTitle").text(params.city);
	$(".impactTitle").text(params.impact);
	$(".indicatorTitle").text(params.indicator);
	chooseScenario(params.scn);
	//chooseImpact(params.indicator);
//	createScenario1(currentParam);
	
	
});

function ChangeScenario(scn){
	location.href = location.href.replace(/=scn\d/gi, "="+scn);
}

function dropdownScenari(descrScen){
	var count = 0;
	var liDrop="";
	for (var i in descrScen) {
		if (descrScen.hasOwnProperty(i)) {
			count++;
			var descr = descrScen[i];
			console.log('<li class="'+i+'"><a class="dcr'+count+'" href="javascript:ChangeScenario(\''+i+'\')">'+descr+'</a></li>'); 
			liDrop=liDrop+'<li class="'+i+'"><a class="dcr'+count+'" href="javascript:ChangeScenario(\''+i+'\')">'+descr+'</a></li>';
		}
	}
	$('#dropdownScenari').html(liDrop)
	
}





function chooseScenario(scn){
	var $target = $('.cockpitIframe').clone().removeClass("hide");

	switch (scn) { 
		case "scn1": 
			console.log('scn1 Wins!');
			//scenario1
			createScenario1(currentParam);
			$('#dropdownScenari .scn1').addClass("active");
			break;
		case "scn2": 
			console.log('scn2 Wins!');
			createScenario2(currentParam);
			$('#dropdownScenari .scn2').addClass("active");
			break;
		case "scn3": 
			console.log('scn3 Wins!');
			createScenario3(currentParam);
			$('#dropdownScenari .scn3').addClass("active");
			break;		
		case "scn4": 
			console.log('scn4 Wins!');
			createScenario4(currentParam);
			$('#dropdownScenari .scn4').addClass("active");
			break;
		case "scn5": 
			console.log('scn5 Wins!');
			createScenario5(currentParam);
			$('#dropdownScenari .scn5').addClass("active");
			break;
		case "scn6": 
			console.log('scn6 Wins!');
			createScenario6(currentParam);
			$('#dropdownScenari .scn6').addClass("active");
			break;
		default:
			console.log('Nobody Wins!');
	}
}

//Eindhoven
function createScenario1(currentP){
	var $target = $('.cockpitIframe').clone().removeClass("hide");

	switch (currentP) { 
		case paramsComb1: 
			console.log('SC1comb1 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhls.replace('attributo=xxx','attributo=Direct%20Impacts').replace('attributo_field_visible_description=x', 'attributo_field_visible_description=Direct%20Impacts')));
			//$('#scnSelect').hide();
			dropdownScenari(descriptionHeat);
			break;
		case paramsComb2: 
			console.log('SC1comb2 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhns.replace('selettore=xxx','selettore=Direct%20Impacts%20(absolute%20values)').replace('selettore_field_visible_description=x','selettore_field_visible_description=Direct%20Impacts%20(absolute%20values)')));
			//$('#scnSelect').hide();
			dropdownScenari(descriptionHeatNeigh);
			break;
		case paramsComb3: 
			console.log('SC1comb3 Wins!');
			$('#singleCockpit').hide();
			//scenario1
			$target.clone().appendTo('#multiCockpit .cockpitBox');
			$target.clone().appendTo('#tab1').attr("src",USG_scn1LandUse);
			$('.tab1 .tabName').html("Land Use");
			$target.clone().appendTo('#tab2').attr("src",USG_scn1HouseHT);
			$('.tab2 .tabName').html("HOUSE HOLD TYPE");
			$target.clone().appendTo('#tab3').attr("src",USG_scenario1Q18);
			$('.tab3 .tabName').html("OTHERS INDICATORS");
			dropdownScenari(descriptionSprawl);
			break;		
		case paramsComb4: 
			console.log('SC1comb4 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src", USNscAllinOne.replace('SocEc_SX_2015_Neigh','SocEc_S1_2015_Neigh').replace('Scenario%20X','Scenario%201')));
			dropdownScenari(descriptionSprawlN);
			break;
		case paramsComb13:
			console.log('SC1comb13 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhci.replace('selettore=xxx','selettore=Direct%20Impacts').replace('selettore_field_visible_description=x','selettore_field_visible_description=Direct%20Impacts')));
			//$('#scnSelect').hide();
			dropdownScenari(descriptionHeat);
			break;
		default:
			console.log('SC1Nobody Wins!');
	}
}





//Eindhoven
function createScenario2(currentP){
	var $target = $('.cockpitIframe').clone().removeClass("hide");

	switch (currentP) { 
		case paramsComb1: 
			console.log('SC2comb1 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhls.replace('attributo=xxx','attributo=Indirect%20Impacts').replace('attributo_field_visible_description=x', 'attributo_field_visible_description=Indirect%20Impacts')));
			//$('#scnSelect').hide();
			dropdownScenari(descriptionHeat);
			break;
		case paramsComb2: 
			console.log('SC1comb2 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhns.replace('selettore=xxx','selettore=Direct%20Impacts%20(difference%20values)').replace('selettore_field_visible_description=x','selettore_field_visible_description=Direct%20Impacts%20(difference%20values)')));
			dropdownScenari(descriptionHeatNeigh);
			break;
		case paramsComb3: 
			console.log('SC2comb3 Wins!');
			//scenario2
			$('#singleCockpit').hide();
			$target.clone().appendTo('#multiCockpit .cockpitBox');
			$target.clone().appendTo('#tab1').attr("src",USG_SC2_LANDUSE);
			$('.tab1 .tabName').html("Land Use");
			$target.clone().appendTo('#tab2').attr("src",USG_SCN2HOUSEHT);
			$('.tab2 .tabName').html("HOUSE HOLD TYPE");
			$target.clone().appendTo('#tab3').attr("src",USGSC2_INDICATORS);
			$('.tab3 .tabName').html("OTHERS INDICATORS");
			dropdownScenari(descriptionSprawl);
			break;		
		case paramsComb4: 
			console.log('SC2comb4 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src", USNscAllinOne.replace('SocEc_SX_2015_Neigh','SocEc_S2_2015_Neigh').replace('Scenario%20X','Scenario%202')));
			dropdownScenari(descriptionSprawlN);
			break;
		case paramsComb13:
			console.log('SC2comb13 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhci.replace('selettore=xxx','selettore=Indirect%20Impacts').replace('selettore_field_visible_description=x','selettore_field_visible_description=Indirect%20Impacts')));
			//$('#scnSelect').hide();
			dropdownScenari(descriptionHeat);
			break;
		default:
			console.log('SC2Nobody Wins!');
	}
}

//Eindhoven
function createScenario3(currentP){
	var $target = $('.cockpitIframe').clone().removeClass("hide");

	switch (currentP) { 
		case paramsComb1: 
			console.log('SC3comb1 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhls.replace('attributo=xxx','attributo=Direct%20and%20Indirect%20Impacts').replace('attributo_field_visible_description=x', 'attributo_field_visible_description=Direct%20and%20Indirect%20Impacts')));
			//$('#scnSelect').hide();
			dropdownScenari(descriptionHeat);
			break;
		case paramsComb2: 
			console.log('SC1comb2 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhns.replace('selettore=xxx','selettore=Indirect%20Impacts%20(absolute%20values)').replace('selettore_field_visible_description=x','selettore_field_visible_description=Indirect%20Impacts%20(absolute%20values)')));
			dropdownScenari(descriptionHeatNeigh);
			break;
		case paramsComb3: 
			console.log('SC3comb3 Wins!');
			//scenario3
			$('#singleCockpit').hide();
			$target.clone().appendTo('#multiCockpit .cockpitBox')
			$target.clone().appendTo('#tab1').attr("src",USGsc3LandUse);
			$('.tab1 .tabName').html("Land Use");
			$target.clone().appendTo('#tab2').attr("src",USGsc3HOUSEHT);
			$('.tab2 .tabName').html("HOUSE HOLD TYPE");
			$target.clone().appendTo('#tab3').attr("src",USGsc3Indicators);
			$('.tab3 .tabName').html("OTHERS INDICATORS");
			dropdownScenari(descriptionSprawl);
			break;		
		case paramsComb4: 
			console.log('SC3comb4 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src", USNscAllinOne.replace('SocEc_SX_2015_Neigh','SocEc_S3_2015_Neigh').replace('Scenario%20X','Scenario%203')));
			dropdownScenari(descriptionSprawlN);
			break;
		case paramsComb13:
			console.log('SC2comb13 Wins!');
			$('#multiCockpit').hide();
			$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhci.replace('selettore=xxx','selettore=Direct%20and%20Indirect%20Impacts').replace('selettore_field_visible_description=x','selettore_field_visible_description=Direct%20and%20Indirect%20Impacts')));
			//$('#scnSelect').hide();
			dropdownScenari(descriptionHeat);
			break;
		default:
			console.log('SC3Nobody Wins!');
	}
}
	
	//Eindhoven
	function createScenario4(currentP){
		var $target = $('.cockpitIframe').clone().removeClass("hide");

		switch (currentP) { 
			case paramsComb1: 
				console.log('SC4comb1 Wins!');
				break;
			case paramsComb2: 
				console.log('SC1comb2 Wins!');
				$('#multiCockpit').hide();
				$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhns.replace('selettore=xxx','selettore=Indirect%20Impacts%20(difference%20values)').replace('selettore_field_visible_description=x','selettore_field_visible_description=Indirect%20Impacts%20(difference%20values)')));
				dropdownScenari(descriptionHeatNeigh);
				break;
			case paramsComb3: 
				console.log('SC4comb3 Wins!');
				//scenario4
				$('#singleCockpit').hide();
				$target.clone().appendTo('#multiCockpit .cockpitBox')
				$target.clone().appendTo('#tab1').attr("src",USGsc4LandUse);
				$('.tab1 .tabName').html("Land Use");
				$target.clone().appendTo('#tab2').attr("src",USGsc4HOUSEHT);
				$('.tab2 .tabName').html("HOUSE HOLD TYPE");
				$target.clone().appendTo('#tab3').attr("src",USGsc4Indicators);
				$('.tab3 .tabName').html("OTHERS INDICATORS");
				dropdownScenari(descriptionSprawl);
				break;		
			case paramsComb4: 
				console.log('SC4comb4 Wins!');
				$('#multiCockpit').hide();
				$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src", USNscAllinOne.replace('SocEc_SX_2015_Neigh','SocEc_S4_2015_Neigh').replace('Scenario%20X','Scenario%204')));
				dropdownScenari(descriptionSprawlN);
				break;
			default:
				console.log('SC4Nobody Wins!');
		}
}
	
	//Eindhoven
	function createScenario5(currentP){
		var $target = $('.cockpitIframe').clone().removeClass("hide");

		switch (currentP) { 
			case paramsComb1: 
				console.log('SC5comb1 Wins!');
				break;
			case paramsComb2: 
				console.log('SC1comb2 Wins!');
				$('#multiCockpit').hide();
				$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhns.replace('selettore=xxx','selettore=Direct%20and%20Indirect%20Impacts%20(absolute%20values)').replace('selettore_field_visible_description=x','selettore_field_visible_description=Direct%20and%20Indirect%20Impacts%20(absolute%20values)')));
				dropdownScenari(descriptionHeatNeigh);
				break;
			case paramsComb3: 
				console.log('SC5comb3 Wins!');
				//scenario5
				break;		
			case paramsComb4: 
				console.log('SC5comb4 Wins!');
				$('#multiCockpit').hide();
				$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src", USNscAllinOne.replace('SocEc_SX_2015_Neigh','SocEc_S5_2015_Neigh').replace('Scenario%20X','Scenario%205')));
				dropdownScenari(descriptionSprawlN);
				break;
			default:
				console.log('SC5Nobody Wins!');
		}
	}
	
	//Eindhoven
	function createScenario6(currentP){
		var $target = $('.cockpitIframe').clone().removeClass("hide");

		switch (currentP) { 
			case paramsComb1: 
				console.log('SC5comb1 Wins!');
				break;
			case paramsComb2: 
				console.log('SC6comb2 Wins!');
				$('#multiCockpit').hide();
				$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src",uhns.replace('selettore=xxx','selettore=Direct%20and%20Indirect%20Impacts%20(difference%20values)').replace('selettore_field_visible_description=x','selettore_field_visible_description=Direct%20and%20Indirect%20Impacts%20(difference%20values)')));
				dropdownScenari(descriptionHeatNeigh);
				break;
			case paramsComb3: 
				console.log('SC5comb3 Wins!');
				//scenario5
				break;		
			case paramsComb4: 
				console.log('SC5comb4 Wins!');
				$('#multiCockpit').hide();
				$('#singleCockpit .cockpitBox').replaceWith($target.clone().attr("src", USNscAllinOne.replace('USNxxx','USNsc5').replace('ScenarioX','Scenario5')));
				dropdownScenari(descriptionSprawlN);
				break;
			default:
				console.log('SC5Nobody Wins!');
		}
	}

	