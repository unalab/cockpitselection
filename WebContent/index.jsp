<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<meta charset="ISO-8859-1">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<title>NBS SVT tool</title>
	
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	
	<link rel="stylesheet" type="text/css" href="css/fullpage.min.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />

	<!--[if IE]>
		<script type="text/javascript">
			 var console = { log: function() {} };
		</script>
	<![endif]-->

</head>
<body>
<div class="overlap">
  <div class="preloader-wrapper big active">
    <div class="spinner-layer spinner-primary-only">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div><div class="gap-patch">
        <div class="circle"></div>
      </div><div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div>
</div>

<nav id="header">
	<div class="nav-wrapper white">
		<a href="index.jsp" class="brand-logo"><img src="imgs/logo_alpha_270x.png" class="logo" alt="UNaLab logo"></a>
		
		 <span class="brand-logo secondary-text" style="margin-left: 130px; margin-top: 12px;">NBS Simulation Visualization Tool</span>
		<!--
		<ul class="right">
			<li><a class="grey-text text-darken-4" >Create ONIA Problem</a></li>
		</ul>
		-->
	</div>
</nav>

<div id="fullpage">
	<div class="section" id="section0">
<!-- slide -->
		<%@include file="slides/slide_1.jspf"%>
	
	    <%@include file="slides/slide_2.jspf"%>

   	    <%@include file="slides/slide_3.jspf"%>

<%--     	<%@include file="slides/slide_4.jspf"%> --%>
<!-- end slide -->
		
	</div>
<!-- 	section -->
</div>
<!-- fullpage -->

<!-- Modal Structure -->
  <div id="modal1" class="modal bottom-sheet">
    <div class="modal-content" id="modal-description" >
     
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-light btn left-align">Ok</a>
    </div>
  </div>
		

<footer class="valign-wrapper primary">
	<div class="center white-text">&copy; UNALAB project</div>
</footer>
	
	
	<!--JavaScript at end of body for optimized loading-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script type="text/javascript" src="vendors/scrolloverflow.min.js"></script>
	<script type="text/javascript" src="js/fullpage.min.js"></script>
	<script type="text/javascript" charset="ISO-8859-1" src="js/variable.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	
	<script type="text/javascript">
	  $(document).ready(function(){
		    $('.modal').modal();
		    $('.tabs').tabs();
		    $('.collapsible').collapsible();
		    $( ".collapsible-header" ).click(function() {
		        $(".more",this).toggle()
		        $(".less", this).toggle()
		    });
		  });
	       
	</script>

</body>
</html>